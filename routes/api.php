<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->get(
    '/waiting',
    [
        'as'   => 'keywords.waiting',
        'uses' => 'KeywordController@waiting',
    ]
);

$router->get(
    '/value/{keyword}',
    [
        'as'   => 'keywords.translations',
        'uses' => 'TranslationController@translations',
    ]
);

$router->post(
    '/value/{keyword}[/{language}]',
    [
        'as'   => 'keywords.translation',
        'uses' => 'TranslationController@translation',
    ]
);

$router->post(
    '/translate/{keyword}/{language}',
    [
        'as'   => 'keywords.translate',
        'uses' => 'TranslationController@translate',
    ]
);