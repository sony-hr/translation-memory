<?php

namespace App\Rules;

use App\Repositories\TranslationRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UniqueTranslation implements Rule
{
    /**
     * @var TranslationRepository
     */
    private $repository;

    /**
     * @var string
     */
    private $keyword;

    /**
     * @var string
     */
    private $languageCode;

    /**
     * Create a new rule instance.
     *
     * @param TranslationRepository $repository
     * @param string $keyword
     * @param string $languageCode
     */
    public function __construct(TranslationRepository $repository, string $keyword, string $languageCode)
    {
        $this->repository   = $repository;
        $this->keyword      = $keyword;
        $this->languageCode = $languageCode;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $this->repository->retrieveTranslate($this->keyword, $this->languageCode);
        } catch (ModelNotFoundException $exception) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param $attribute
     * @param $value
     *
     * @return bool
     */
    public function validate($attribute, $value)
    {
        return $this->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute already exist.';
    }
}
