<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <habil@sanalpara.com>
 * Date: 2019-03-25
 * Time: 01:47
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class TranslationResource
 *
 * @package App\Http\Resources
 */
class TranslationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'link' => [
                'self' => 'translations',
            ],
        ];
    }
}