<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <habil@sanalpara.com>
 * Date: 2019-03-25
 * Time: 01:47
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TranslationResource
 *
 * @property string $keyword
 * @property array $languages
 *
 * @package App\Http\Resources
 */
class WaitingKeyword extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'keyword'   => $this->keyword,
            'languages' => $this->languages,
        ];
    }
}