<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <habil@sanalpara.com>
 * Date: 2019-03-25
 * Time: 01:47
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TranslationResource
 *
 * @property int $id
 * @property string $translate
 * @property int $keyword_id
 * @property string $language_code
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \App\Keyword $keyword
 * @property \App\Language $language
 *
 * @package App\Http\Resources
 */
class Translation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'translate' => $this->translate,
            'keyword'   => $this->keyword->keyword,
            'language'  => new Language($this->language),
        ];
    }
}