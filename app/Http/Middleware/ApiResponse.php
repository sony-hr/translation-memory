<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class ApiResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($response instanceof JsonResponse) {
            $content = $response->getData();

            if (!empty($content) && !isset($content->data) && !isset($content->errors)) {
                if ($response->status() < 400) {
                    return $response->setData(['data' => $response->getData()]);
                } else {
                    return $response->setData(['errors' => $response->getData()]);
                }
            }
        }

        return $response;
    }
}
