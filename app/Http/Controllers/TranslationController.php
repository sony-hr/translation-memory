<?php

namespace App\Http\Controllers;

use App\Http\Resources\TranslationCollection;
use App\Http\Resources\Translation as TranslationResource;
use App\Repositories\KeywordRepository;
use App\Repositories\TranslationRepository;
use App\Rules\UniqueTranslation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TranslationController extends Controller
{
    /**
     * @var \App\Repositories\KeywordRepository
     */
    protected $keywordRepository;

    /**
     * @var \App\Repositories\TranslationRepository
     */
    protected $translationRepository;

    /**
     * Create a new controller instance.
     *
     * @param KeywordRepository     $keywordRepository
     * @param TranslationRepository $translationRepository
     */
    public function __construct(KeywordRepository $keywordRepository, TranslationRepository $translationRepository)
    {
        $this->keywordRepository     = $keywordRepository;
        $this->translationRepository = $translationRepository;
    }

    /**
     * Retrieve translations of keyword in all exist languages
     *
     * @param string $keyword
     *
     * @return TranslationCollection
     */
    public function translations(string $keyword)
    {
        return new TranslationCollection($this->keywordRepository->retrieveByKeyword($keyword)->translations);
    }

    /**
     * Retrieve translations of keyword in all exist languages
     *
     * @param string $keyword
     * @param string $language
     *
     * @return TranslationResource
     */
    public function translation(string $keyword, string $language = '')
    {
        return new TranslationResource($this->translationRepository->retrieveTranslateOrStoreWaiting($keyword, $language));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $keyword
     * @param string                   $language
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function translate(Request $request, string $keyword, string $language)
    {
        $this->validate(
            $request,
            [
                'translate' => [
                    'required',
                    'string',
                    new UniqueTranslation($this->translationRepository, $keyword, $language),
                ],
            ]
        );

        $this->translationRepository->storeTranslation($request->input('translate'), $keyword, $language);

        return response()->json([], Response::HTTP_CREATED);
    }
}
