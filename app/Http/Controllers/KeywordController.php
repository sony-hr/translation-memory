<?php

namespace App\Http\Controllers;

use App\Http\Resources\WaitingKeywordCollection;
use App\Repositories\KeywordRepository;

class KeywordController extends Controller
{
    /**
     * @var \App\Repositories\KeywordRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @param \App\Repositories\KeywordRepository $repository
     */
    public function __construct(KeywordRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Retrieve all keywords does not have translation
     *
     * @return WaitingKeywordCollection
     */
    public function waiting()
    {
        return new WaitingKeywordCollection($this->repository->retrieveAllTranslateWaiting());
    }
}
