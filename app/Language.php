<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 14:48
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 *
 * @property int id
 * @property string name
 * @property string code
 *
 * @package App
 */
class Language extends Model
{
    const LANGUAGE_CODE_LENGTH = '5';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
    ];

    /**
     * Retrieve translation related language
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(Translation::class);
    }
}