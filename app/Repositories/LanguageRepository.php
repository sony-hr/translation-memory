<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 21:45
 */

namespace App\Repositories;

use App\Keyword;
use App\Language;

class LanguageRepository extends RepositoryAbstract
{
    /**
     * @var \App\Language
     */
    protected $model;

    /**
     * LanguageRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Language());
    }

    /**
     * Retrieve language with code
     *
     * @param $code
     *
     * @return \App\Language
     */
    public function retrieveLanguageByCode(string $code): Language
    {
        return $this->model->where('code', $code)->first();
    }
}