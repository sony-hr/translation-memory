<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 21:45
 */

namespace App\Repositories;

use App\Exceptions\KeywordNotFoundException;
use App\Translation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TranslationRepository extends RepositoryAbstract
{
    /**
     * @var \App\Translation
     */
    protected $model;

    /**
     * Translation Repository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Translation());
    }

    /**
     * @param string $translate
     * @param string $keyword
     * @param string $languageCode
     *
     * @return mixed
     */
    public function storeTranslation(string $translate, string $keyword, string $languageCode)
    {
        $keywordRepository = (new KeywordRepository());

        try {
            $keyword = $keywordRepository->retrieveByKeyword($keyword);
        } catch (ModelNotFoundException $exception) {
            $keyword = $keywordRepository->storeKeyword($keyword);
        }

        return $this->store(
            [
                'translate'     => $translate,
                'keyword_id'    => $keyword->id,
                'language_code' => $languageCode,
            ]
        );
    }

    /**
     * Retrieve Keyword translation in specific language
     *
     * @param string $keyword
     * @param string $code
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function retrieveTranslate($keyword, $code): Model
    {
        $keyword = (new KeywordRepository())->retrieveByKeyword($keyword);

        return $this->model->where(['keyword_id' => $keyword->id, 'language_code' => $code])->firstOrFail();
    }

    /**
     * @param string $keyword
     * @param string $language
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function retrieveTranslateOrStoreWaiting(string $keyword, string $language = '')
    {
        // TODO This method should implement in controller layer
        //   and if new keyword stored response with 201 HTTP status code
        //   now it's creating new keyword but return 404 status code
        try {
            return $this->retrieveTranslate($keyword, $language);
        } catch (KeywordNotFoundException $exception) {
            (new KeywordRepository())->storeKeyword($keyword);

            throw new KeywordNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }
}