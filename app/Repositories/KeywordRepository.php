<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 21:45
 */

namespace App\Repositories;

use App\Exceptions\KeywordNotFoundException;
use App\Keyword;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class KeywordRepository extends RepositoryAbstract
{
    /**
     * @var \App\Keyword
     */
    protected $model;

    /**
     * KeywordRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Keyword());
    }

    /**
     * Store new keyword
     *
     * @param string $keyword
     *
     * @return mixed
     */
    public function storeKeyword(string $keyword)
    {
        return $this->store(['keyword' => $keyword]);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function retrieveAllTranslateWaiting()
    {
        $languages = (new LanguageRepository())->all();

        $waitingKeywords = $this->model->retrieveAllTranslateWaiting($languages->count());

        // Retrieve all language codes and map all fetched keywords
        // check if languages field has value means keyword has some
        // translations but not in all languages we exclude exist languages
        // from list and send left ones to response, Otherwise the keyword
        // should translate in all languages and does not have any translation
        $languages = $languages->pluck('code');
        $waitingKeywords->map(function ($keyword) use ($languages) {
            if (!empty($keyword->languages)) {
                $existTranslationsLang = collect(explode(',', $keyword->languages));
                $keyword->languages    = $languages->diff($existTranslationsLang)->toArray();
            } else {
                $keyword->languages = $languages->toArray();
            }

            return $keyword;
        });

        return $waitingKeywords;
    }

    /**
     * @param string $keyword
     *
     * @return mixed
     */
    public function retrieveByKeyword(string $keyword)
    {
        try {
            return $this->model->where('keyword', $keyword)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            throw new KeywordNotFoundException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function retrieveWaitingKeywordTranslations()
    {
        $existTranslationLanguages = $this->model->translations()->get()->pluck('language_code');

        return (new LanguageRepository())->getModel()->whereNotIn('code', $existTranslationLanguages)->get();
    }
}