<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 21:47
 */

namespace App\Repositories;

use Illuminate\Support\Collection;

interface RepositoryInterface
{
    /**
     * @return Collection
     */
    public function all();

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data);

    /**
     * @param array $data
     * @param $id
     *
     * @return mixed
     */
    public function update(array $data, $id);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function show($id);
}