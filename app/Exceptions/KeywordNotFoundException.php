<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <habil@sanalpara.com>
 * Date: 2019-03-25
 * Time: 01:09
 */

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class KeywordNotFoundException extends ModelNotFoundException
{

}