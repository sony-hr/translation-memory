<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 14:48
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Keyword
 *
 * @property string $keyword
 * @property \Illuminate\Database\Eloquent\Collection $translations
 *
 * @package App
 */
class Keyword extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'keywords';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'keyword',
    ];

    /**
     * Retrieve keyword related translations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(Translation::class);
    }

    /**
     * @param int $languagesCount
     *
     * @return Collection
     */
    public static function retrieveAllTranslateWaiting(int $languagesCount)
    {
        // Get all Keywords that does not have translation
        // or not translated in all languages
        return DB::table('keywords')
            ->leftJoin('translations', 'keywords.id', '=', 'translations.keyword_id')
            ->leftJoin('languages', 'translations.language_code', '=', 'languages.code')
            ->select('keywords.id', 'keywords.keyword')
            ->selectRaw('array_to_string(array_agg(languages.code), \',\') as languages')
            ->havingRaw("count(translations.id) < {$languagesCount}")
            ->groupBy('keywords.id')
            ->get();
    }
}