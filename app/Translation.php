<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 14:48
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Translation
 *
 * @property int $id
 * @property string $translate
 * @property int $keyword_id
 * @property string $language_code
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \App\Keyword $keyword
 * @property \App\Language $language
 *
 * @package App
 */
class Translation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'translate',
        'keyword_id',
        'language_code',
    ];

    /**
     * Retrieve translation related keyword
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function keyword()
    {
        return $this->belongsTo(Keyword::class);
    }

    /**
     * Retrieve translation related language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_code', 'code');
    }
}