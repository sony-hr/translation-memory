<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('translate');
            $table->unsignedBigInteger('keyword_id');
            $table->string('language_code', \App\Language::LANGUAGE_CODE_LENGTH);
            $table->timestamps();

            $table->foreign('keyword_id')
                ->references('id')
                ->on('keywords');

            $table->foreign('language_code')
                ->references('code')
                ->on('languages');

            $table->unique(['keyword_id', 'language_code'], 'keyword__language_code__unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translations');
    }
}
