<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Translation::class, function (Faker\Generator $faker) {
    return [
        'translate'     => implode(' ', $faker->words('3')),
        'keyword_id'    => function () {
            return factory(\App\Keyword::class)->create()->id;
        },
        'language_code' => function () {
            return factory(\App\Language::class)->create()->code;
        },
    ];
});