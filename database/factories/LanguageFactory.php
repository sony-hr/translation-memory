<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Language::class, function (Faker\Generator $faker) {
    $languages = [
        'tr' => 'Turkish',
        'en' => 'English',
        'fr' => 'French',
        'sp' => 'Spanish',
        'fa' => 'Farsi',
    ];
    $rndKey    = $faker->randomKey($languages);

    return [
        'name' => $languages[$rndKey],
        'code' => $rndKey,
    ];
});
