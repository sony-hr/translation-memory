<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 13:04
 */

class LanguagesTableSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            'tr' => 'Turkish',
            'en' => 'English',
            'fr' => 'French',
            'sp' => 'Spanish',
            'fa' => 'Farsi',
        ];

        foreach ($languages as $code => $language) {
            factory(\App\Language::class)->create([
                'name' => $language,
                'code' => $code,
            ]);
        }
    }
}