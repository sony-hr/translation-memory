<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 13:04
 */

class TranslationsTableSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translations = [];

        foreach (\App\Language::all() as $language) {
            $translations[] = factory(\App\Translation::class)->make([
                'keyword_id'    => NULL,
                'language_code' => $language->code,
            ]);
        }
        $keywords = \App\Keyword::all();

        $keywords[1]->translations()->saveMany($translations);

        $keywords->first()->translations()->saveMany([
            factory(\App\Translation::class)->make([
                'translate'     => 'Merhaba Dunya',
                'keyword_id'    => NULL,
                'language_code' => 'tr',
            ]),
            factory(\App\Translation::class)->make([
                'translate'     => 'Hello World',
                'keyword_id'    => NULL,
                'language_code' => 'en',
            ]),
        ]);
    }
}