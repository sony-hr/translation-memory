# Installation  
## Build & Run:  
This command will build docker images and run containers from docker-compose.yml file.
Run this command under root folder of project.  

`docker-compose up --build -d`  

## Setup:
This command will install composer packages and run migrations and seeders.  

`docker exec -it translation_memory_php composer run-script setup`

## Test
Run this command to test project (Unit & Feature tests).
`docker exec -it translation_memory_php ./vendor/bin/phpunit`

## Refresh DB & Seed:
You will need this command after running test because it will clear you DB after running test.
Note: At the beginning i used in memory SQLite for tests but because of a GET /waiting query 
i used some PostgreSQL select extensions because of that i changed the test database driver 
from SQLite to PostgreSQL.  
 
`docker exec -it translation_memory_php php artisan migrate:refresh --seed`

# Endpoints
The complete list of endpoints is exist in 
**./docs/postman-api-collection/translation-memory.postman_collection.json**
[Online Documents](https://documenter.getpostman.com/view/1425563/S17tQ7ZL)

### Translation Waiting
`GET /waiting`   
List of all keywords that waiting for translation in all languages.

### Translations List
`GET /value/[KEYWORD]`   
List of translations of sent keyword in exist languages.

### Translation Value
`POST /value/[KEYWORD]/[LANGUAGE_CODE]`   
Retrieve translation of keyword in specific language if exist, Otherwise it will create a keyword without any 
translation to show in translation waiting list.

### Translate Value
`POST /translate/[KEYWORD]/[LANGUAGE_CODE]`   
Store translation of keyword in specific language.