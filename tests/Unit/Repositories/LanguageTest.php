<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 15:22
 */

namespace Tests\Unit\Repositories;

use App\Repositories\LanguageRepository;
use Tests\Factories\Traits\LanguageFactory;
use Tests\TestCase;

class LanguageTest extends TestCase
{
    use LanguageFactory;

    /**
     * @test
     */
    public function can_retrieve_language_by_code()
    {
        $language = $this->createLanguage('tr', 'Turkish');

        $this->assertEquals($language->code, (new LanguageRepository())->retrieveLanguageByCode('tr')->code);
    }
}