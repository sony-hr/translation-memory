<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 15:22
 */

namespace Tests\Unit\Repositories;

use App\Exceptions\KeywordNotFoundException;
use App\Keyword;
use App\Repositories\TranslationRepository;
use App\Translation;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\Factories\Traits\LanguageFactory;
use Tests\TestCase;

class TranslationTest extends TestCase
{
    use LanguageFactory;

    /**
     * @var TranslationRepository
     */
    private $repository;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new TranslationRepository();
    }

    /**
     * @test
     */
    public function can_store_new_translate_for_exist_keyword()
    {
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $this->createFiveLanguages();

        $this->repository->storeTranslation('Hello World', $keyword->keyword, 'en');

        $this->seeInDatabase('translations', ['translate' => 'Hello World']);
    }

    /**
     * @test
     */
    public function can_store_new_translate_for_none_exist_keyword()
    {
        $this->createFiveLanguages();

        $this->repository->storeTranslation('Hello World', 'hello_world', 'en');

        $this->seeInDatabase('translations', ['translate' => 'Hello World']);
    }

    /**
     * @test
     */
    public function can_retrieve_keyword_translation_in_specific_language()
    {
        $this->createFiveLanguages();
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $keyword->translations()->save(factory(Translation::class)->make([
            'translate'     => 'Merhaba Dunya',
            'language_code' => 'tr',
        ]));

        $this->assertEquals('Merhaba Dunya', $this->repository->retrieveTranslate($keyword->keyword, 'tr')->translate);
    }

    /**
     * @test
     */
    public function can_retrieve_translate_of_exist_translation_keyword()
    {
        $this->createFiveLanguages();
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $keyword->translations()->save(factory(Translation::class)->make([
            'translate'     => 'Merhaba Dunya',
            'language_code' => 'tr',
        ]));

        $this->assertEquals(
            'Merhaba Dunya',
            $this->repository->retrieveTranslateOrStoreWaiting($keyword->keyword, 'tr')->translate
        );
    }

    /**
     * @test
     */
    public function expect_exception_for_translate_of_not_exist_translation_keyword()
    {
        $this->expectExceptionObject(new KeywordNotFoundException('No query results for model [App\Keyword].'));

        $this->repository->retrieveTranslateOrStoreWaiting('hello_world', 'sp');
    }

    /**
     * @test
     */
    public function can_retrieve_translate_of_not_exist_translation_keyword_in_language()
    {
        $this->expectExceptionObject(new ModelNotFoundException('No query results for model [App\Translation].'));

        $this->createFiveLanguages();
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $keyword->translations()->save(factory(Translation::class)->make([
            'translate'     => 'Merhaba Dunya',
            'language_code' => 'tr',
        ]));

        $this->repository->retrieveTranslateOrStoreWaiting($keyword->keyword, 'sp');
    }
}