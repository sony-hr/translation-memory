<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 15:22
 */

namespace Tests\Unit\Repositories;

use App\Keyword;
use App\Language;
use App\Repositories\KeywordRepository;
use App\Translation;
use Tests\TestCase;
use Tests\Factories\Traits\LanguageFactory;

class KeywordTest extends TestCase
{
    use LanguageFactory;

    /**
     * @var \App\Keyword
     */
    private $keyword;

    /**
     * @var \App\Repositories\KeywordRepository
     */
    private $repository;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new KeywordRepository();
    }

    /**
     * @test
     */
    public function can_store_new_keyword()
    {
        $this->repository->storeKeyword('hello_world');

        $this->seeInDatabase('keywords', ['keyword' => 'hello_world']);
    }

    /**
     * @test
     */
    public function can_retrieve_all_waiting_translations()
    {
        $keywords = factory(Keyword::class, 5)->create();
        $this->createFiveLanguages();

        $translations = [];
        foreach (Language::all() as $language) {
            $translations[] = factory(Translation::class)->make([
                'keyword_id'    => NULL,
                'language_code' => $language->code,
            ]);
        }

        $keywords->first()->translations()->saveMany($translations);

        $keywords[1]->translations()->save(factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => 'tr',
        ]));
        $keywords[1]->translations()->save(factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => 'sp',
        ]));

        $this->assertCount(4, $waitingKeywords = $this->repository->retrieveAllTranslateWaiting());
    }

    /**
     * @test
     */
    public function can_retrieve_waiting_translations()
    {
        $this->keyword = factory(Keyword::class)->create();
        $this->createFiveLanguages();

        $translations[] = factory(Translation::class)->make(['language_code' => 'tr']);
        $translations[] = factory(Translation::class)->make(['language_code' => 'en']);
        $translations[] = factory(Translation::class)->make(['language_code' => 'sp']);
        $this->keyword->translations()->saveMany($translations);

        $this->repository->setModel($this->keyword);
        $waitingLanguages = $this->repository->retrieveWaitingKeywordTranslations();

        $this->assertCount(2, $waitingLanguages);
        $this->assertEquals(['fr', 'fa'], $waitingLanguages->pluck('code')->toArray());
    }

    /**
     * @test
     */
    public function can_retrieve_keyword_by_keyword_itself()
    {
        $this->keyword = factory(Keyword::class)->create();

        $this->assertEquals($this->keyword->id, $this->repository->retrieveByKeyword($this->keyword->keyword)->id);
    }
}