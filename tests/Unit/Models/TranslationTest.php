<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 15:22
 */

namespace Tests\Unit\Models;

use Tests\Factories\Traits\LanguageFactory;
use Tests\TestCase;

class TranslationTest extends TestCase
{
    use LanguageFactory;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function can_retrieve_translation_language()
    {
        $language = $this->createLanguage('tr', 'Turkish');

        $translation = factory(\App\Translation::class)->create(['language_code' => $language->code]);

        $this->assertEquals($language->code, $translation->language->code);
    }
}