<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 15:22
 */

namespace Tests\Unit\Models;

use App\Keyword;
use App\Translation;
use Tests\TestCase;
use Tests\Factories\Traits\LanguageFactory;

class KeywordTest extends TestCase
{
    use LanguageFactory;

    /**
     * @var \App\Keyword
     */
    private $keyword;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function can_retrieve_translation_of_keyword()
    {
        $this->keyword = factory(Keyword::class)->create();

        $translation = factory(Translation::class)->make();

        $this->keyword->translations()->save($translation);

        $this->assertEquals(Translation::find(1)->id, $this->keyword->translations()->first()->id);
    }

    /**
     * @test
     */
    public function can_retrieve_translations_of_keyword_in_all_languages()
    {
        $this->keyword = factory(Keyword::class)->create();
        $this->createFiveLanguages();

        $translations[] = factory(Translation::class)->make(['language_code' => 'tr']);
        $translations[] = factory(Translation::class)->make(['language_code' => 'en']);
        $translations[] = factory(Translation::class)->make(['language_code' => 'sp']);

        $this->keyword->translations()->saveMany($translations);

        $this->seeInDatabase('translations', Translation::find(1)->toArray());
        $this->assertEquals('sp', $this->keyword->translations->last()->language->code);
        $this->assertEquals('tr', $this->keyword->translations->first()->language->code);
    }
}