<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 03:16
 */

namespace Tests\Feature;

use App\Keyword;
use App\Language;
use App\Translation;
use Tests\Factories\Traits\LanguageFactory;
use Tests\TestCase;

class KeywordTest extends TestCase
{
    use LanguageFactory;

    /**
     * @test
     */
    public function can_retrieve_translate_waiting_keywords_and_languages()
    {
        $keywords = factory(Keyword::class, 5)->create();
        $this->createFiveLanguages();

        $translations = [];
        foreach (Language::all() as $language) {
            $translations[] = factory(Translation::class)->make([
                'keyword_id'    => NULL,
                'language_code' => $language->code,
            ]);
        }

        $keywords->first()->translations()->saveMany($translations);

        $keywords[1]->translations()->save(factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => 'tr',
        ]));
        $keywords[1]->translations()->save(factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => 'sp',
        ]));

        $response = $this->get(route('keywords.waiting'));
        $content  = json_decode($response->response->getContent(), TRUE);

        $response->assertResponseOk();
        $response->seeJsonContains(['keyword' => $keywords[2]->keyword]);
        $this->assertCount(4, $content['data']);
    }
}