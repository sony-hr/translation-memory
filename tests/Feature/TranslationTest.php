<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-24
 * Time: 03:16
 */

namespace Tests\Feature;

use App\Keyword;
use App\Language;
use App\Translation;
use Illuminate\Http\Response;
use Tests\Factories\Traits\LanguageFactory;
use Tests\TestCase;

class TranslationTest extends TestCase
{
    use LanguageFactory;

    /**
     * @test
     */
    public function can_retrieve_keyword_translations_list()
    {
        $keyword = factory(Keyword::class)->create();
        $this->createFiveLanguages();

        $translations = [];
        foreach (Language::all() as $language) {
            $translations[] = factory(Translation::class)->make([
                'keyword_id'    => NULL,
                'language_code' => $language->code,
            ]);
        }
        $keyword->first()->translations()->saveMany($translations);

        $response = $this->get(route('keywords.translations', ['keyword' => $keyword->keyword]));

        $response->assertResponseOk();
        $response->seeJsonContains(['translate' => $translations[0]->translate]);
    }

    /**
     * @test
     */
    public function can_retrieve_keyword_translation_in_specific_language()
    {
        $keyword     = factory(Keyword::class)->create();
        $language    = $this->createLanguage('tr', 'Turkish');
        $translation = factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => $language->code,
        ]);
        $keyword->first()->translations()->save($translation);

        $response = $this->post(route(
            'keywords.translation',
            [
                'keyword'  => $keyword->keyword,
                'language' => 'tr',
            ]
        ));

        $response->assertResponseOk();
        $response->seeJsonContains([
            'data' => [
                'translate' => $translation->translate,
                'keyword'   => $keyword->keyword,
                'language'  => [
                    'code' => $language->code,
                    'name' => $language->name,
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function expect_error_for_not_exist_keyword_translation_in_specific_language()
    {
        $response = $this->post(route(
            'keywords.translation',
            [
                'keyword'  => 'hello_world',
                'language' => 'tr',
            ]
        ));

        $response->assertResponseStatus(Response::HTTP_NOT_FOUND);
        $response->seeJsonContains([
            'code'    => 0,
            'message' => 'No query results for model [App\\Keyword].',
        ]);
    }

    /**
     * @test
     */
    public function can_store_new_translation_for_exist_keyword()
    {
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $this->createFiveLanguages();

        $response = $this->post(
            route('keywords.translate', ['keyword' => $keyword->keyword, 'language' => 'en']),
            ['translate' => 'Hello World']
        );

        $response->assertResponseStatus(Response::HTTP_CREATED);
        $this->seeInDatabase('translations', ['translate' => 'Hello World']);
    }

    /**
     * @test
     */
    public function can_store_new_translation_for_none_exist_keyword()
    {
        $this->createFiveLanguages();

        $response = $this->post(
            route('keywords.translate', ['keyword' => 'hello_world', 'language' => 'en']),
            ['translate' => 'Hello World']
        );

        $response->assertResponseStatus(Response::HTTP_CREATED);
        $this->seeInDatabase('translations', ['translate' => 'Hello World']);
    }

    /**
     * @test
     */
    public function expect_validation_error_if_translation_not_provided()
    {
        $this->createFiveLanguages();

        $response = $this->post(route('keywords.translate', ['keyword' => 'hello_world', 'language' => 'en']));

        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->notSeeInDatabase('translations', ['translate' => 'Hello World']);
    }

    /**
     * @test
     */
    public function expect_validation_error_if_translation_exist()
    {
        $keyword = factory(Keyword::class)->create(['keyword' => 'hello_world']);
        $this->createFiveLanguages();
        $keyword->translations()->save(factory(Translation::class)->make([
            'keyword_id'    => NULL,
            'language_code' => 'en',
            'translate'     => 'Hello World',
        ]));

        $response = $this->post(
            route('keywords.translate', ['keyword' => 'hello_world', 'language' => 'en']),
            ['translate' => 'Hello World']
        );

        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeInDatabase('translations', ['translate' => 'Hello World']);
    }
}