<?php
/**
 * Created by PhpStorm.
 * Project translation-memory
 * Author: Siavash Habil <amirkhiz@gmail.com>
 * Date: 2019-03-23
 * Time: 19:49
 */

namespace Tests\Factories\Traits;

use App\Language;

trait LanguageFactory
{
    protected $languages = [
        'tr' => 'Turkish',
        'en' => 'English',
        'fr' => 'French',
        'sp' => 'Spanish',
        'fa' => 'Farsi',
    ];

    /**
     * Generate single language
     *
     * @param $code
     * @param $name
     *
     * @return mixed
     */
    public function createLanguage($code, $name)
    {
        return factory(Language::class)->create([
            'name' => $name,
            'code' => $code,
        ]);
    }

    /**
     * Generate Five languages
     */
    public function createFiveLanguages()
    {
        foreach ($this->languages as $code => $language) {
            $this->createLanguage($code, $language);
        }
    }

    /**
     * Retrieve language with code
     *
     * @param $code
     *
     * @return \App\Language
     */
    public function retrieveLanguage($code): Language
    {
        return Language::where('code', $code)->first();
    }
}